module.exports = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  roots: ["api", "ui"],
  testMatch: ["<rootDir>/(ui|api)/**/?(*.)test.{ts,tsx}"],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  verbose: true,
  globals: {
    window: true,
    document: true,
    navigator: true,
    "ts-jest": {
      tsConfig: "./ui/tsconfig.json"
    }
  },
  automock: false,
  setupFiles: ["./setupJest.js"]
};
