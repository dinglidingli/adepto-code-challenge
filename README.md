# Hamsterest

**tl;dr** I did (most of) the thing.

A Heroku version of this lives at: [https://hamsterest.herokuapp.com/](https://hamsterest.herokuapp.com/)

## The problem

Hamsters want blogs. They _really_ want them. So much so, that Adepto has discovered a peculiarly niche market which they want to capitalize on. Bring on: Hamsterest (because Hamstergram was taken).

Three user stories pertaining to the problem were presented:

- As a hamster, I want to sign up to a platform so I can connect with other the hamsters on the network
- As a social hamster, I want to be able to share posts with hamsters in the network
- As a social hamster I want the hamsters in the network to like my posts

From this, the following requirements could be inferred:

- Sign up to the site
- Log in to the site
- Connect with other Hamsters
- Remove a connection with another Hamster
- Share posts to the network
- Edit posts
- Allow users to like posts
- Allow users to unlike posts

The solution towards providing this functionality was to produce two distinctly separate applications (though both have been included in this repository for brevity) which isolate logic pertaining to both the user interface as well as the underlying server application.

For obvious reasons, not all of these have been implemented but the foundation to build out all of these features was included in both the API and UI application structures.

## How the project was completed

The application is split into two separate applications; it has an API and UI.

I had originally wanted to use Docker to containerise the separate components of the application. I ended up scrapping this because my experience in that area is quite low and I wanted to focus on producing the application.

### API

The API is a basic Express application built atop Typescript, Express and TypeORM with a SQLite database for storing content. The architectural approach was to produce a RESTful API which followed a standardised format allowing for easier implementation of the front-end.

Functionality is split between a few key components:

- `router.ts` to redirect requests to the appropriate controllers
- `controller/x.ts` to handle the appropriate requests. I had originally decided to make the controllers as singletons as it would have allowed me to use Typescript decorators in situations where I wanted to transform the response bodies prior to returning them. Because the nature of the application was quite simple, I left all of the application logic within the controllers. In an ideal situation, I would have preferred to move the actual application logic to a `services` directory.
- `entity/x.ts` to handle the interactions with TypeORM and SQLite. I like using TypeORM because it's the most flexible ORM I have worked with in recent times.
- `middleware/ErrorMiddleware.ts` was used in `app.ts` to catch application errors and return them as a response. It's a very basic method which just outputs the error in JSON format.
- `middleware/UserMiddleware.ts` is used in each request prior to that request being routed to a controller. Its function is quite simple: to validate a JWT and fetch that user's details from Auth0 as well as the database. Given more time, I would have liked to make this more robust and handled a number of other situations like checking the validity of the provided token.

I'd left out a number of endpoints that I would typically introduce into a RESTful API; particularly, the remaining operations handled by CRUD for each of the entities. Idid do a few naughty things like include the entire entity's information (including relationships) in the list responses instead of only providing the high level object (which is preferable).

I left out a lot of code comments in the API section. To be completely honest, I don't see the point with a lot of Typescript written code as it is effectively self-documenting. In places where the logic isn't obvious, I have also included inline commentary for clarification.

There also weren't any places that I saw real value in testing for as most tests for those scenarios in the API are protected against through Typescript's strict typing anyway. Most application logic was centred around building the TypeORM models and executing the save / find methods against them.

### UI

The API is a React application coded in Typescript for its strict typing capabilities (and with that, the added benefit of easier issue finding). I also used the React Hooks APIs that were introduced in React 16.8.x (for no other reason than for also wanting to learn more about them as I hadn't used them prior to this project).

The directory structure is quite simple, where the components are separated by the top-level function that corresponds to an API route (ie, Hamster, Post). The 'long-term' goal was to maintain consistent naming if I were to implement similar functionalities at a later date. Basically, I wanted:

- A list view
- A list item view
- A create view

Each of these views maintained responsibility for doing the relevant network requests to the API.

Because I didn't need the full power of a library like Redux or MobX for this project, I instead used React's new `useReducer()` method to create a small, simple reducer that could handle dispatch events and store the data.

With using these new APIs this came a few issues which unfortunately weren't documented all too well on the official documentation for React. In particular, I learned that the new Reducer API doesn't keep updating state for all consuming components when a new event is dispatched. Instead, the correct approach was to implement the reducer in the parent and then pass the `state` and `dispatch` objects to the children. This probably could have been cleaned up more with the Context API, but I didn't feel like the time investment into investigating that was worth it for the sake of this test.

I ended up not implementing the contact functionality in `Hamster/HamsterList.tsx` purely because I didn't feel it would really add much extra value to the purposes of this test.

Adding extra unit tests to the UI would also have been beneficial to ensuring code quality. While Typescript is a godsend for catching a number of code quality problems, there are a few situations that would benefit from more testing:

- Extra snapshot tests to ensure that a component's output remains correct despite different combinations of inputs.
- Test interaction responses using Enzyme to confirm that interactions trigger the same responses they should

I'm particularly proud of the code that lives in `Hamster/HamsterList.tsx` because it _was_ my first foray into using the new Hook APIs, and I enjoyed writing it because of how much considerably cleaner the code actually became. Without using classes, I no longer had to implement similar accessors for the same data in multiple places. Hooks also allowed me to not have to do things like connect my method to a Redux store.
