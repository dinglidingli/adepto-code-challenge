import { BaseEntity, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Hamster } from "./Hamster";
import { Post } from "./Post";

@Entity()
export class Like extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(() => Hamster, (hamster: Hamster) => hamster.likes, {
    nullable: false
  })
  hamster!: Hamster;

  @ManyToOne(() => Post, (post: Post) => post.likes, { nullable: false })
  post!: Post;
}
