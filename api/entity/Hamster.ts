import {
  BaseEntity,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryColumn,
  Column
} from "typeorm";
import { Like } from "./Like";
import { Post } from "./Post";

@Entity()
export class Hamster extends BaseEntity {
  @PrimaryColumn()
  id!: string;

  @Column({
    default: ""
  })
  name!: string;

  @Column({
    // I found this site looking for a random image generator...
    default: "http://junglebiscuit.com/images/random/rand_image.pl"
  })
  pictureUri!: string;

  @Column()
  email!: string;

  @ManyToMany(() => Hamster, (hamster: Hamster) => hamster.contactsInverse, {
    cascade: true
  })
  @JoinTable()
  contacts!: Hamster[];

  @ManyToMany(() => Hamster, (hamster: Hamster) => hamster.contacts)
  contactsInverse!: Hamster[];

  @OneToMany(() => Post, (post: Post) => post.hamster)
  posts!: Post[];

  @OneToMany(() => Like, (like: Like) => like.hamster)
  likes!: Like[];
}
