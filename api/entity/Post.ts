import { Hamster } from "./Hamster";
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  Column
} from "typeorm";
import { Like } from "./Like";

@Entity()
export class Post extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  title!: string;

  @Column()
  content!: string;

  @ManyToOne(() => Hamster, (hamster: Hamster) => hamster.posts, {
    nullable: false
  })
  hamster!: Hamster;

  @OneToMany(() => Like, (like: Like) => like.post)
  likes!: Like[];
}
