import { NextFunction } from "connect";
import { Request, Response } from "express";
import { Like } from "../entity/Like";
import { Post } from "../entity/Post";

class LikeController {
  public async create(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    const { postId } = req.params;

    const like: Like = new Like();
    like.hamster = res.locals.hamster;

    try {
      like.post = await Post.findOneOrFail({
        where: { id: postId },
        relations: ["likes", "likes.hamster"]
      });
    } catch (e) {
      return next(e);
    }

    if (!like.post.likes.find((l: Like) => l.hamster.id === like.hamster.id)) {
      try {
        await like.save();
      } catch (e) {
        return next(e);
      }
    }

    return res.json(like);
  }
}

export default new LikeController();
export { LikeController };
