import { Request, Response } from "express";
import { NextFunction } from "connect";
import { Hamster } from "../entity/Hamster";

class HamsterController {
  public async list(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> {
    const hamsters: Hamster[] = await Hamster.find({
      relations: ["contacts"]
    });

    return res.json(hamsters);
  }

  public async get(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    const { hamsterId } = req.params;

    try {
      const hamster: Hamster = await Hamster.findOneOrFail(hamsterId);
      return res.json(hamster);
    } catch (e) {
      return next(e);
    }
  }

  public async put(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    const { hamsterId } = req.params;

    if (res.locals.hamster.id !== hamsterId) {
      const err: Error = new Error("Unauthorized");
      return next(err);
    }

    const { action } = req.query;

    const hamster: Hamster = res.locals.hamster;

    switch (action) {
      case "addContact":
        const { contactHamsterId } = req.query;

        // Don't duplicate contacts
        if (hamster.contacts.find((c: Hamster) => c.id === contactHamsterId)) {
          break;
        }

        try {
          const contactHamster: Hamster = await Hamster.findOneOrFail(
            contactHamsterId
          );

          hamster.contacts.push(contactHamster);
        } catch (e) {
          return next(e);
        }
        break;
      // Do other things if we add extra actions to this method
    }

    try {
      hamster.save();
      return res.json(hamster);
    } catch (e) {
      return next(e);
    }
  }
}

export default new HamsterController();
export { HamsterController };
