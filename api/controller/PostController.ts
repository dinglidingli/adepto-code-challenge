import { NextFunction } from "connect";
import { Request, Response } from "express";
import { Post } from "../entity/Post";

class PostController {
  public async list(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> {
    const posts: Post[] = await Post.find({
      relations: ["hamster", "likes"]
    });
    return res.json(posts);
  }

  public async get(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    const { postId } = req.params;

    try {
      const post: Post = await Post.findOneOrFail(postId, {
        relations: ["hamster", "likes"]
      });
      return res.json(post);
    } catch (e) {
      return next(e);
    }
  }

  public async create(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response | void> {
    const { title, content } = req.body;
    const post: Post = new Post();
    post.likes = [];
    post.title = title;
    post.content = content;
    post.hamster = res.locals.hamster;

    try {
      await post.save();
    } catch (e) {
      return next(e);
    }

    return res.json(post);
  }
}

export default new PostController();
export { PostController };
