import { NextFunction, Request, Response } from "express";

export function ErrorMiddleware(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) {
  // This isn't going live so it doesn't really matter
  // if we are going to dump the error details here
  res.status(500).json({
    error: "Something broke!",
    details: err
  });
}
