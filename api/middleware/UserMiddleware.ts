import { AuthenticationClient } from "auth0";
import { Hamster } from "./../entity/Hamster";
import { NextFunction, Request, Response } from "express";
import jwt_decode from "jwt-decode";

export async function UserMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> {
  if (!req.path.includes("api") || req.method === "GET") {
    return next();
  }

  const token: string | null = req.headers.authorization
    ? req.headers.authorization.replace("Bearer:", "").trim()
    : null;

  if (!token || token === "") {
    const error: Error = new Error("Unauthorized");
    return next(error);
  }

  const [atPair, itPair] = token.split("|");
  const accessToken = atPair.split("=")[1];
  const idToken = itPair.split("=")[1];

  const decodedJwt: any = jwt_decode(idToken);
  const sub: string = decodedJwt.sub;

  let hamster: Hamster | undefined = await Hamster.findOne(sub);

  if (!hamster) {
    const auth0 = new AuthenticationClient({
      clientId: "3UOlekB4GluPpzoXvieYCMZv7XoAY9AV",
      clientSecret:
        "le0Xqs-eCmrOWNr_n53hQUvoj46IcvR-ZLDHVe4PcpBT6IcBpUkXdalht0Z1Llrh",
      domain: "hamsterest.au.auth0.com"
    });

    const userProfile = await auth0.getProfile(accessToken);

    hamster = new Hamster();
    hamster.id = sub;
    hamster.name = userProfile.given_name;
    hamster.pictureUri = userProfile.picture;
    hamster.email = userProfile.email;

    try {
      hamster.save();
    } catch (e) {
      return next(e);
    }
  }

  res.locals.hamster = hamster;

  return next();
}
