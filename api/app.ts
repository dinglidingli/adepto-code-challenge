import { json } from "body-parser";
import express, { Application } from "express";
import Bundler from "parcel-bundler";
import { join } from "path";
import { ErrorMiddleware } from "./middleware/ErrorMiddleware";
import { UserMiddleware } from "./middleware/UserMiddleware";
import { router } from "./router";

const bundlerUiPath: string = join(__dirname, "..", "ui", "index.html");
const bundlerOptions: Bundler.ParcelOptions = {};

const bundler: Bundler = new Bundler(bundlerUiPath, bundlerOptions);
const app: Application = express();

app.use(UserMiddleware);

app.use(json());

app.use("/api/", router);
app.use(bundler.middleware());

app.use(ErrorMiddleware);

export { app };
