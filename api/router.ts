import { Router } from "express";
import HamsterController from "./controller/HamsterController";
import PostController from "./controller/PostController";
import LikeController from "./controller/LikeController";

const router: Router = Router();

router.get("/hamster", HamsterController.list);
router.get("/hamster/:hamsterId", HamsterController.get);
router.put("/hamster/:hamsterId", HamsterController.put);

router.get("/post", PostController.list);
router.get("/post/:postId", PostController.get);
router.post("/post", PostController.create);

router.post("/post/:postId/like", LikeController.create);

export { router };
