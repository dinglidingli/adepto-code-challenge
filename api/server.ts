/**
 * This is a scaled back version of the application
 * server typically provided by the express generator
 * to only include functionality important for the purposes
 * of this test.
 */

import { createServer, Server } from "http";
import "reflect-metadata";
import { createConnection } from "typeorm";
import { app } from "./app";

let server: Server;

createConnection()
  .then(onConnection)
  .catch(error => console.log(error));

async function onConnection(): Promise<void> {
  server = createServer(app);
  server.listen(process.env.PORT || 3000);
  server.on("listening", onListening);
}

function onListening(): void {
  const addr: string | { port: number; family: string; address: string } =
    server.address() || "";

  console.log("Listening on", typeof addr === "string" ? addr : addr.port);
}
