import Cookies from "js-cookie";
import { IKVPair } from "./components/Authentication/Authentication";

/**
 * Takes an array of IKVPair objects and returns the value for the specifed key
 *
 * @param pairs
 * @param key
 */
export function getValueFromKVPair(pairs: IKVPair[] = [], key: string): string {
  const pair: IKVPair | undefined = pairs.find(
    (pair: IKVPair) => pair.key === key
  );

  return pair ? pair.value : "";
}

/**
 * An abstraction over the browser's fetch() API
 *
 * This abstraction will:
 * - Insert the authentication details token into each request
 * - Submit the request as application/json
 * - Automatically include the protocol and host to contact the API
 *   and prevent the need to define a full path when consuming this
 *   method.
 * - Detect status errors
 * - Return the JSON response
 *
 * @param info
 * @param init
 */
export async function fetchUtil(
  info: RequestInfo,
  init: RequestInit = {}
): Promise<any> {
  if (typeof info === "string") {
    const { protocol, host } = window.location;
    info = `${protocol}//${host}/api${info}`;
  }

  let bearerToken: string = "";
  if (Cookies.getJSON("authenticationDetails")) {
    const authenticationDetails: IKVPair[] = Cookies.getJSON(
      "authenticationDetails"
    );

    const accessToken: string = getValueFromKVPair(
      authenticationDetails,
      "access_token"
    );

    const idToken: string = getValueFromKVPair(
      authenticationDetails,
      "id_token"
    );

    bearerToken = `at=${accessToken}|it=${idToken}`;
  }

  init.headers = {
    ...init.headers,
    Authorization: `Bearer: ${bearerToken}`,
    "Content-Type": "application/json"
  };

  const response: Response = await fetch(info, init);
  const status: number = response.status;
  const body: any = await response.json();

  if (status.toString().charAt(0) !== "2") {
    throw new Error("Fetch failed");
  }

  return {
    status,
    body
  };
}
