import {
  Avatar,
  Button,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import React, { useEffect, useReducer } from "react";
import { SET_HAMSTERS } from "../../constants";
import { fetchUtil } from "../../utilities";
import { defaultState, reducer } from "../App/reducer";
import { IKVPair } from "../Authentication/Authentication";

interface IProps {
  authenticationDetails: IKVPair[];
}

/**
 * Render a list of Hamster users
 *
 * @param props HamsterList component props
 */
export function HamsterList(props: IProps) {
  const { authenticationDetails } = props;
  const [{ hamsters }, dispatch] = useReducer(reducer, defaultState);

  useEffect(() => {
    const controller = new AbortController();
    const { signal } = controller;

    fetchUtil("/hamster", { signal })
      .then(response => {
        dispatch({
          type: SET_HAMSTERS,
          hamsters: response.body
        });
      })
      .catch(e => {
        // This will usually fail as the result of the AbortController
        // but for the purposes of time I'm just not going to handle the
        // errors properly here.
      });

    return () => controller.abort();
  }, []);

  return (
    <List style={{ maxWidth: 600, margin: "0 auto", marginTop: 16 }}>
      {hamsters.map((hamster: any, index: number) => {
        let contactButton = authenticationDetails && (
          <Button
            onClick={() => {
              // Handle the request
            }}
          >
            Add as contact
          </Button>
        );

        // If the user exists in the list of contacts, then show a 'remove contact' button
        // ^ This is really unsound pseudo logic. We should _never_ expose another Hamster's
        //   contacts unless we want to allow turning Hamsters into Catfish.

        return (
          <ListItem key={`hamster_${index}`}>
            <Avatar alt={hamster.name} src={hamster.pictureUri} />
            <ListItemText primary={hamster.name} />
            {contactButton}
          </ListItem>
        );
      })}
    </List>
  );
}
