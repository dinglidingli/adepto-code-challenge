import React from "react";
import renderer from "react-test-renderer";
import { HamsterList } from "./HamsterList";

describe("HamsterList", () => {
  it("should match its snapshots", () => {
    const tree1 = renderer
      .create(<HamsterList authenticationDetails={[]} />)
      .toJSON();

    const tree2 = renderer
      .create(
        <HamsterList
          authenticationDetails={[
            { key: "a", value: "a" },
            { key: "b", value: "b" }
          ]}
        />
      )
      .toJSON();

    expect(tree1).toMatchSnapshot();
    expect(tree2).toMatchSnapshot();
  });
});
