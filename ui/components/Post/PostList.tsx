import React, { useEffect, useReducer } from "react";
import { SET_POSTS } from "../../constants";
import { fetchUtil } from "../../utilities";
import { defaultState, reducer } from "../App/reducer";
import { PostCreator } from "./PostCreator";
import { Post } from "./Post";
import { IKVPair } from "../Authentication/Authentication";

interface IProps {
  authenticationDetails: IKVPair[];
}

/**
 * Render a list of Post objects and also provide the Create form
 * if the user is authenticated.
 *
 * @param props PostList component props
 */
export function PostList(props: IProps) {
  const { authenticationDetails } = props;
  const [{ posts }, dispatch] = useReducer(reducer, defaultState);

  useEffect(() => {
    const controller = new AbortController();
    const { signal } = controller;

    fetchUtil("/post", { signal })
      .then(response => {
        dispatch({
          type: SET_POSTS,
          posts: response.body
        });
      })
      .catch(e => {
        // This will usually fail as the result of the AbortController
        // but for the purposes of time I'm just not going to handle the
        // errors properly here.
      });

    return () => controller.abort();
  }, []);

  return (
    <div style={{ maxWidth: 600, margin: "0 auto", marginTop: 16 }}>
      {authenticationDetails && <PostCreator dispatch={dispatch} />}

      {posts.map((post: any) => (
        <Post
          key={`post_${post.id}`}
          post={post}
          dispatch={dispatch}
          canLike={!!authenticationDetails}
        />
      ))}
    </div>
  );
}
