import {
  Button,
  TextField,
  Card,
  CardContent,
  CardHeader
} from "@material-ui/core";
import React, { Dispatch, useEffect, useState } from "react";
import { SET_POSTS } from "../../constants";
import { fetchUtil } from "../../utilities";

// We pass dispatch as a prop instead of using useReducer because
// this component is handled as a child of PostList, which means
// that triggering a new instance of dispatch won't cause a re-render on the
// parent.
interface IProps {
  dispatch: Dispatch<any>;
}

/**
 * Take user inputs and submit them to the API to create a new Post.
 *
 * @param props PostCreator component props
 */
export function PostCreator(props: IProps): JSX.Element {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [submittingPost, setSubmittingPost] = useState(false);

  useEffect(() => {
    if (submittingPost && (title || setTitle)) {
      fetchUtil("/post", {
        method: "POST",
        body: JSON.stringify({
          title,
          content
        })
      }).then(response => {
        props.dispatch({
          type: SET_POSTS,
          posts: [response.body]
        });

        setTitle("");
        setContent("");
        setSubmittingPost(false);
      });
    }
  }, [submittingPost]);

  return (
    <Card style={{ marginBottom: 16 }}>
      <CardHeader title="Create a new post" />
      <CardContent>
        <form>
          <TextField
            fullWidth
            variant="outlined"
            id="title"
            label="Title"
            value={title}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setTitle(event.currentTarget.value)
            }
            margin="dense"
          />

          <TextField
            fullWidth
            id="content"
            variant="outlined"
            label="Content"
            multiline
            value={content}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setContent(event.currentTarget.value)
            }
            margin="normal"
          />

          <Button
            color="primary"
            onClick={() => setSubmittingPost(true)}
            style={{ marginTop: 16 }}
          >
            Create Post
          </Button>
        </form>
      </CardContent>
    </Card>
  );
}
