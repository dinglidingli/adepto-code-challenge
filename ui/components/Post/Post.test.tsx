import React from "react";
import renderer from "react-test-renderer";
import { Post } from "./Post";

describe("Post", () => {
  const dispatch = jest.fn();
  const mockPost = {
    id: 1,
    title: "Title",
    content: "Content",
    hamster: {
      id: 1,
      name: "Mr Hamster"
    },
    likes: []
  };

  beforeEach(() => {
    dispatch.mockReset();
  });

  it("should match its snapshots", () => {
    const tree1 = renderer
      .create(<Post canLike={false} dispatch={dispatch} post={mockPost} />)
      .toJSON();

    expect(tree1).toMatchSnapshot();

    const tree2 = renderer
      .create(<Post canLike={true} dispatch={dispatch} post={mockPost} />)
      .toJSON();

    expect(tree2).toMatchSnapshot();
  });
});
