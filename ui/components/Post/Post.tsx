import React, { Dispatch, useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  CardActions,
  IconButton,
  Avatar,
  Badge
} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { fetchUtil } from "../../utilities";
import { LIKE_POST } from "../../constants";

// I couldn't be bothered actually defining the interface
// beyond the objects being returned by the API, sorry.
interface IProps {
  post: any;
  dispatch: Dispatch<any>;
  canLike: boolean;
}

/**
 * Render an individual post
 *
 * @param props Post component props
 */
export function Post(props: IProps): JSX.Element {
  const { post, canLike } = props;
  const [likingPost, setLikingPost] = useState(false);

  useEffect(() => {
    if (likingPost) {
      fetchUtil(`/post/${post.id}/like`, {
        method: "POST"
      }).then(() => {
        props.dispatch({
          type: LIKE_POST,
          postId: post.id
        });

        setLikingPost(false);
      });
    }
  }, [likingPost]);

  return (
    <Card style={{ marginBottom: 16 }}>
      <CardHeader
        title={`${post.title} by ${post.hamster.name}`}
        avatar={
          <Avatar
            alt={post.hamster.name}
            src={post.hamster.pictureUri}
            title={`Posted by ${post.hamster.name}`}
          />
        }
      />
      <CardContent>
        <Typography component="p">{post.content}</Typography>
      </CardContent>
      <CardActions>
        <Badge badgeContent={post.likes.length} color="primary">
          <IconButton
            aria-label="Like post"
            onClick={() => {
              if (canLike) {
                setLikingPost(true);
              }
            }}
          >
            <FavoriteIcon />
          </IconButton>
        </Badge>
      </CardActions>
    </Card>
  );
}
