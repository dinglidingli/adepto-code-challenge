import {
  AppBar,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Toolbar,
  Typography
} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import MenuIcon from "@material-ui/icons/Menu";
import React, { useReducer, useState } from "react";
import { BrowserRouter, Link, Route } from "react-router-dom";
import { Authentication } from "../Authentication";
import { HamsterList } from "../Hamster/HamsterList";
import { PostList } from "../Post/PostList";
import { defaultState, reducer } from "./reducer";

/**
 * App component used as a top-level React element that gets rendered to the page
 */
export function App(): JSX.Element {
  const [{ authenticationDetails }, dispatch] = useReducer(
    reducer,
    defaultState
  );
  const [drawerOpen, setDrawerOpen] = useState(false);

  return (
    <React.Fragment>
      <CssBaseline />

      <BrowserRouter>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Menu"
              onClick={() => setDrawerOpen(!drawerOpen)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" style={{ flexGrow: 1 }}>
              Hamsterest
            </Typography>
            <Authentication
              dispatch={dispatch}
              authenticationDetails={authenticationDetails}
            />
          </Toolbar>
        </AppBar>

        <Drawer open={drawerOpen} onClose={() => setDrawerOpen(false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={() => setDrawerOpen(false)}
            onKeyDown={() => setDrawerOpen(false)}
          >
            <List>
              <Link to="/" style={{ textDecoration: "none" }}>
                <ListItem button>
                  <ListItemText primary="Home" />
                </ListItem>
              </Link>
              <Link to="/hamster" style={{ textDecoration: "none" }}>
                <ListItem button>
                  <ListItemText primary="Hamsters" />
                </ListItem>
              </Link>
            </List>
          </div>
        </Drawer>

        <Route
          exact
          path="/"
          component={() => (
            <PostList authenticationDetails={authenticationDetails} />
          )}
        />
        <Route
          path="/hamster"
          exact
          component={() => (
            <HamsterList authenticationDetails={authenticationDetails} />
          )}
        />
      </BrowserRouter>
    </React.Fragment>
  );
}
