import Cookies from "js-cookie";
import {
  LIKE_POST,
  SET_AUTHENTICATION_DETAILS,
  SET_HAMSTERS,
  SET_POSTS
} from "../../constants";

export interface IHamsterestState {
  hamsters: any[];
  posts: any[];
  authenticationDetails: any;
}

export const defaultState: IHamsterestState = {
  hamsters: [],
  posts: [],
  authenticationDetails: Cookies.getJSON("authenticationDetails")
};

/**
 * A simple reducer function that is compatible with React's useReducer() method
 *
 * @param state
 * @param action
 */
export function reducer(
  state: IHamsterestState,
  action: any
): IHamsterestState {
  switch (action.type) {
    case SET_AUTHENTICATION_DETAILS: {
      const { authenticationDetails } = action;
      Cookies.set("authenticationDetails", authenticationDetails);

      return {
        ...state,
        authenticationDetails
      };
    }
    case SET_HAMSTERS: {
      const { hamsters } = action;

      return {
        ...state,
        hamsters
      };
    }
    case SET_POSTS: {
      const { posts } = action;

      return {
        ...state,
        posts: [...state.posts, ...posts].sort((a: any, b: any) => b.id - a.id)
      };
    }
    case LIKE_POST: {
      const { postId } = action;
      const { posts } = state;
      const post: any = posts.find((p: any) => p.id === postId);

      if (post.likes.filter((p: any) => p.id === "TEMP").length === 0) {
        post.likes.push({ id: "TEMP" });
      }

      return {
        ...state,
        posts: [...posts]
      };
    }
    default:
      return state;
  }
}
