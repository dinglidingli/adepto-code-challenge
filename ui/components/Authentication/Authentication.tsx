import { Button } from "@material-ui/core";
import auth0 from "auth0-js";
import Cookies from "js-cookie";
import React, { Dispatch, useEffect } from "react";
import { SET_AUTHENTICATION_DETAILS } from "../../constants";

const webAuth: auth0.WebAuth = new auth0.WebAuth({
  domain: "hamsterest.au.auth0.com",
  clientID: "3UOlekB4GluPpzoXvieYCMZv7XoAY9AV",
  responseType: "token id_token",
  scope: "openid profile email",
  redirectUri: `${window.location.protocol}//${window.location.host}`
});

export interface IKVPair {
  key: string;
  value: string;
}

/**
 * Converts a hash string to KeyValue pairs
 *
 * @param locationHash A hash string as found in window.location.hash
 */
export function getKVPairsFromHash(locationHash: string): IKVPair[] {
  const hash: string = locationHash.replace("#", "");
  const kvPairs: IKVPair[] = hash
    .split("&")
    .filter((kv: string) => !!kv)
    .map(
      (kvPair: string): IKVPair => {
        const [key, value] = kvPair.split("=");
        return { key, value };
      }
    );

  return kvPairs;
}

interface IProps {
  authenticationDetails: IKVPair[];
  dispatch: Dispatch<any>;
}

/**
 * The Authentication component plays a pretty small role:
 * - Kick off the Auth0 authentication process
 * - Ask a user to log in
 * - Ask a user to log out
 *
 * The component utilises the authenticationDetails state as well
 * as dispatch method as provided to it. See the props interface for more
 * information.
 *
 * @param props Authentication component props
 */
export function Authentication(props: IProps): JSX.Element {
  const { authenticationDetails, dispatch } = props;

  useEffect(() => {
    const kvPairs: IKVPair[] =
      Cookies.getJSON("authenticationDetails") ||
      getKVPairsFromHash(window.location.hash);

    window.location.hash = "";

    if (kvPairs.length) {
      dispatch({
        type: SET_AUTHENTICATION_DETAILS,
        authenticationDetails: kvPairs
      });
    }
  }, []);

  if (!authenticationDetails) {
    return (
      <Button
        onClick={() => {
          webAuth.authorize();
        }}
        color="inherit"
      >
        Login
      </Button>
    );
  }

  return (
    <Button
      onClick={() => {
        dispatch({
          type: SET_AUTHENTICATION_DETAILS,
          authenticationDetails: null
        });
      }}
      color="inherit"
    >
      Log out
    </Button>
  );
}
