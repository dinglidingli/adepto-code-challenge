import { getKVPairsFromHash } from "./Authentication";

describe("Authentication", () => {
  it("should parse KeyValue pairs for a given location hash", () => {
    expect(getKVPairsFromHash("#a=aa&b=bb&c=cc")).toEqual([
      { key: "a", value: "aa" },
      { key: "b", value: "bb" },
      { key: "c", value: "cc" }
    ]);

    expect(getKVPairsFromHash("#a=aa&b=bb&c=cc&d")).toEqual([
      { key: "a", value: "aa" },
      { key: "b", value: "bb" },
      { key: "c", value: "cc" },
      { key: "d", value: undefined }
    ]);

    expect(getKVPairsFromHash("#")).toEqual([]);
  });
});
